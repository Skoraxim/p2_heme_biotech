package com.hemebiotech.analytics;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class AnalyticsCounter {
	
	public static void main(String args[]) {

		// Create a reader with symptoms file, read the file and get all symptoms in an ArrayList
		ReadSymptomDataFromFile reader = new ReadSymptomDataFromFile("symptoms.txt");
		ArrayList<String> rawSymptoms = reader.getSymptoms();

		// Create an TreeMap with symptoms as key and their quantity as value
		TreeMap<String, Integer> symptomsMap = rawListToMap(rawSymptoms);

		// Write all the symptoms in result.out at the following format : symptom name = quantity
		WriteSymptomsToFile writer = new WriteSymptomsToFile("result.out", symptomsMap);
		writer.writeToFile();
	}

	

	/**
	 * A simple function which makes a TreeMap containing each symptoms and their quantity.
	 * 
	 * @param rawList a List of symptoms.
	 * @return a TreeMap with each symptoms and their quantity.
	 */
	public static TreeMap<String, Integer> rawListToMap(List<String> rawList) {

		TreeMap<String, Integer> sortedMap = new TreeMap<String, Integer>();
		
		// fill the TreeMap with the symptom and then it's quantity
		for(String symptom : rawList)
		{
			if(sortedMap.containsKey(symptom)) {
				sortedMap.put(symptom, sortedMap.get(symptom) +1);
			} else {
				sortedMap.put(symptom, 1);
			}
		}
		return sortedMap;
	}
}
