package com.hemebiotech.analytics;

/**
 * An interface for WriteSymptomsToFile. The method writeToFile doesn't return and do
 * anything by default.
 */
public interface IWriteToFile {
    void writeToFile();
}
