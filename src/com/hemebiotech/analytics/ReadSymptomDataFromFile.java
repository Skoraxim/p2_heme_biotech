package com.hemebiotech.analytics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Read all symptoms from a file and put all of them in an ArrayList without sorting them
 *
 */
public class ReadSymptomDataFromFile implements ISymptomReader {
	
	private String filepath;
	
	/**
	 * 
	 * @param filepath a full or partial path to file with symptom strings in it, one per line
	 */
	public ReadSymptomDataFromFile (String filepath) {
		this.filepath = filepath;
	}
	
	@Override
	public ArrayList<String> getSymptoms() {
		ArrayList<String> result = new ArrayList<String>();
		BufferedReader reader = null;
		
		if (filepath != null) {
			try {
				reader = new BufferedReader (new FileReader(filepath));
				String line = reader.readLine();
				
				while (line != null) {
					result.add(line);
					line = reader.readLine();
				}
			} catch (IOException e) {
				System.err.println("Unable to read file " + filepath + ". Caused by : " + e.getMessage());
			} finally {
				try {
				if(reader != null) {
					reader.close();
				}
				} catch (IOException e) {
					System.err.println("Unable to close reader. Caused by : " + e.getMessage());
				}
			}
		}
		return result;
	}
}
