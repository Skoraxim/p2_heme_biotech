package com.hemebiotech.analytics;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Object made to take a Map with symptoms and their quantity and
 * write them to a specified file.
 */
public class WriteSymptomsToFile implements IWriteToFile {

    private String filepath;
    private Map<String, Integer> symptoms;

    /**
     * 
     * @param filepath The file which will be written.
     * @param symptoms The Map containing symptoms and their quantity. Each line will be written like this : "symptom name = quantity".
     */
    public WriteSymptomsToFile(String filepath, Map<String, Integer> symptoms) {
        this.filepath = filepath;
        this.symptoms = symptoms;
    }

    @Override
    public void writeToFile() {
        FileWriter writer = null;
        
        if(filepath != null && symptoms != null) {
            try {
                writer = new FileWriter(filepath);
                for(Map.Entry<String, Integer> entry : symptoms.entrySet()) {
                    writer.write(entry.getKey() + " = " + entry.getValue() + "\n");
                }
            } catch (IOException e) {
                System.err.println("Unable to write to result file. Caused by : " + e.getMessage());
            } finally {
                try {
                    if(writer != null) {
                        writer.close();
                    }
                } catch (IOException e) {
                    System.err.println("Unable to close writer. Caused by : " + e.getMessage());
                }
            }
        }
    }
}
